#include "lib/level_deser.hpp"
#include "gtest/gtest.h"

namespace meteo::level_deser::test {
TEST(LevelDeserTest, Empty) {
  std::string body = R"(
  {
    "levels" : []
  }
  )";

  auto levels = deserialize_level(body);

  EXPECT_TRUE(levels.empty());
}

TEST(LevelDeserTest, ValidLevel) {
  std::string body = R"(
  {
    "levels":[
      "0"
    ]
  }
  )";

  auto levels = deserialize_level(body);

  EXPECT_EQ(levels.size(), 1);
  EXPECT_EQ(levels[0].value, 0);
}
}  // namespace meteo::level_deser::test
