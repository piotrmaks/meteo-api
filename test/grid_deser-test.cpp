#include "lib/grid_deser.hpp"
#include "gtest/gtest.h"

namespace meteo::grid_deser::test {
TEST(GridDeserTest, Empty) {
  std::string body = R"(
  {
    "grids" : []
  }
  )";

  auto grids = deserialize_grid(body);

  EXPECT_TRUE(grids.empty());
}

TEST(GridDeserTest, FakeGrid) {
  std::string body = R"(
  {
    "grids":[
      "FakeGrid"
    ]
  }
  )";

  auto grids = deserialize_grid(body);

  EXPECT_EQ(grids.size(), 1);
  EXPECT_EQ(grids[0], std::nullopt);
}

TEST(GridDeserTest, ValidGrid) {
  std::string body = R"(
  {
    "grids":[
      "2a",
      "d01_XLONG_U_XLAT_U",
      "d01_XLONG_V_XLAT_V",
      "d01_XLONG_XLAT",
      "d02_XLONG_U_XLAT_U",
      "d02_XLONG_V_XLAT_V",
      "d02_XLONG_XLAT"
    ]
  }
  )";

  auto grids = deserialize_grid(body);

  EXPECT_EQ(grids.size(), 7);
  EXPECT_TRUE(std::holds_alternative<grid::_2a>(*grids[0]));
  EXPECT_TRUE(std::holds_alternative<grid::d01_XLONG_U_XLAT_U>(*grids[1]));
  EXPECT_TRUE(std::holds_alternative<grid::d01_XLONG_V_XLAT_V>(*grids[2]));
  EXPECT_TRUE(std::holds_alternative<grid::d01_XLONG_XLAT>(*grids[3]));
  EXPECT_TRUE(std::holds_alternative<grid::d02_XLONG_U_XLAT_U>(*grids[4]));
  EXPECT_TRUE(std::holds_alternative<grid::d02_XLONG_V_XLAT_V>(*grids[5]));
  EXPECT_TRUE(std::holds_alternative<grid::d02_XLONG_XLAT>(*grids[6]));
}
}  // namespace meteo::grid_deser::test
