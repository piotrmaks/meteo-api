#include "lib/point_deser.hpp"
#include "gtest/gtest.h"

namespace meteo::point_deser::test {
TEST(PointDeserTest, Empty) {
  std::string body = R"(
  {
    "points" : []
  }
  )";

  auto points = deserialize_point(body);

  EXPECT_TRUE(points.empty());
}

TEST(PointDeserTest, ValidPoint) {
  std::string body = R"(
  {
    "points":[
      {
        "col":255,
        "row":230
      }
    ]
  }
  )";

  auto points = deserialize_point(body);

  EXPECT_EQ(points.size(), 1);
  EXPECT_EQ(points.front().col, 255);
  EXPECT_EQ(points.front().row, 230);
}
}  // namespace meteo::point_deser::test
