#include "lib/field_deser.hpp"
#include "gtest/gtest.h"

namespace meteo::field_deser::test {
TEST(FieldDeserTest, Empty) {
  std::string body = R"(
  {
    "fields" : []
  }
  )";

  auto fields = deserialize_field(body);

  EXPECT_TRUE(fields.empty());
}

TEST(FieldDeserTest, ValidField) {
  std::string body = R"(
  {
    "fields":[
      "ACLWDNT",
      "ACSWDNB"
    ]
  }
  )";

  auto fields = deserialize_field(body);

  EXPECT_EQ(fields.size(), 2);
  EXPECT_EQ(fields[0].field_name, "ACLWDNT");
  EXPECT_EQ(fields[1].field_name, "ACSWDNB");
}
}  // namespace meteo::field_deser::test
