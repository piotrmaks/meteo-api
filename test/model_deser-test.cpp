#include "lib/model_deser.hpp"
#include "gtest/gtest.h"

namespace meteo::model_deser::test {
TEST(ModelDeserTest, Empty) {
  std::string body = R"(
  {
    "models" : []
  }
  )";

  auto models = deserialize_model(body);

  EXPECT_TRUE(models.empty());
}

TEST(ModelDeserTest, FakeModel) {
  std::string body = R"(
  {
    "models":[
      "FakeModel"
    ]
  }
  )";

  auto models = deserialize_model(body);

  EXPECT_EQ(models.size(), 1);
  EXPECT_EQ(models[0], std::nullopt);
}

TEST(ModelDeserTest, ValidModel) {
  std::string body = R"(
  {
    "models":[
      "coamps",
      "wrf"
    ]
  }
  )";

  auto models = deserialize_model(body);

  EXPECT_EQ(models.size(), 2);
  EXPECT_TRUE(std::holds_alternative<model::coamps>(*models[0]));
  EXPECT_TRUE(std::holds_alternative<model::wrf>(*models[1]));
}
}  // namespace meteo::model_deser::test
