# Meteo API

[![Travis Build Status][travis-badge]][travis-url]
[![AppVeyor Build Status][appveyor-badge]][appveyor-url]
[![Buildkite Build Status][buildkite-badge]][buildkite-url]
[![GitLab Build Status][gitlab-badge]][gitlab-url]
[![codecov][codecov-badge]][codecov-url]
[![License: Apache 2.0][licence-badge]](LICENSE)

Nonofficial client for api.meteo.pl written in C++

[travis-badge]: https://travis-ci.com/piotr-maks/meteo-api.svg?branch=master
[travis-url]: https://travis-ci.com/piotr-maks/meteo-api
[appveyor-badge]: https://ci.appveyor.com/api/projects/status/9dkcl0333tayael6/branch/master?svg=true
[appveyor-url]: https://ci.appveyor.com/project/piotr-maks/meteo-api/branch/master
[buildkite-badge]: https://badge.buildkite.com/981d832001df506764935ab5ddb910b6797a70cda4def0c0df.svg?branch=master
[buildkite-url]: https://buildkite.com/bazel/piotr-maks-meteo-api
[gitlab-badge]: https://gitlab.com/piotr.maks/meteo-api/badges/master/pipeline.svg
[gitlab-url]: https://gitlab.com/piotr.maks/meteo-api/commits/master
[codecov-badge]: https://codecov.io/gl/piotr.maks/meteo-api/branch/master/graph/badge.svg
[codecov-url]: https://codecov.io/gl/piotr.maks/meteo-api
[licence-badge]: https://img.shields.io/badge/License-Apache%202.0-green.svg
