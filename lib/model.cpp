#include "lib/model.hpp"

namespace meteo::model {
std::optional<model_t> make_model(std::string_view model_name) {
  if ("coamps" == model_name) {
    return coamps{};
  } else if ("wrf" == model_name)
    return wrf{};

  return std::nullopt;
}
}  // namespace meteo::model
