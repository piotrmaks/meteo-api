#include "lib/field_deser.hpp"

#include <nlohmann/json.hpp>

#include <algorithm>

namespace meteo::field_deser {
using json = nlohmann::json;

field_array deserialize_field(std::string_view body) {
  field_array result{};
  auto parsed = json::parse(body);

  if (auto fields = parsed.find("fields"); fields != parsed.end()) {
    std::transform(fields->begin(), fields->end(), std::back_inserter(result),
                   [](const std::string&& field) {
                     return field::field_t{std::move(field)};
                   });
  }

  return result;
}
}  // namespace meteo::field_deser
