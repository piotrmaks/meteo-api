#pragma once

#include <optional>
#include <string_view>
#include <variant>

namespace meteo::model {
struct coamps {};
struct wrf {};

using model_t = std::variant<coamps, wrf>;

[[nodiscard]] std::optional<model_t> make_model(std::string_view model_name);
}  // namespace meteo::model
