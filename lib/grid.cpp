#include "lib/grid.hpp"

namespace meteo::grid {
std::optional<grid_t> make_grid(std::string_view grid_name) {
  if ("2a" == grid_name) {
    return _2a{};
  } else if ("d01_XLONG_U_XLAT_U" == grid_name) {
    return d01_XLONG_U_XLAT_U{};
  } else if ("d01_XLONG_V_XLAT_V" == grid_name) {
    return d01_XLONG_V_XLAT_V{};
  } else if ("d01_XLONG_XLAT" == grid_name) {
    return d01_XLONG_XLAT{};
  } else if ("d02_XLONG_U_XLAT_U" == grid_name) {
    return d02_XLONG_U_XLAT_U{};
  } else if ("d02_XLONG_V_XLAT_V" == grid_name) {
    return d02_XLONG_V_XLAT_V{};
  } else if ("d02_XLONG_XLAT" == grid_name) {
    return d02_XLONG_XLAT{};
  }
  return std::nullopt;
}
}  // namespace meteo::grid
