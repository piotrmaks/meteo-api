#include "lib/model_deser.hpp"

#include <nlohmann/json.hpp>

#include <algorithm>

namespace meteo::model_deser {
using json = nlohmann::json;

model_array deserialize_model(std::string_view body) {
  model_array result{};
  auto parsed = json::parse(body);

  if (auto models = parsed.find("models"); models != parsed.end()) {
    std::transform(
        models->begin(), models->end(), std::back_inserter(result),
        [](const std::string& model) { return model::make_model(model); });
  }

  return result;
}
}  // namespace meteo::model_deser
