#pragma once

#include "lib/level.hpp"

#include <string_view>
#include <vector>

namespace meteo::level_deser {
using level_array = std::vector<level::level_t>;

[[nodiscard]] level_array deserialize_level(std::string_view body);
}  // namespace meteo::level_deser
