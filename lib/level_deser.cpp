#include "lib/level_deser.hpp"

#include <nlohmann/json.hpp>

#include <algorithm>
#include <string>

namespace meteo::level_deser {
using json = nlohmann::json;

level_array deserialize_level(std::string_view body) {
  level_array result{};
  auto parsed = json::parse(body);

  if (auto levels = parsed.find("levels"); levels != parsed.end()) {
    std::transform(levels->begin(), levels->end(), std::back_inserter(result),
                   [](const std::string& level) {
                     return level::level_t{std::stoi(level)};
                   });
  }

  return result;
}
}  // namespace meteo::level_deser
