#pragma once

#include "lib/grid.hpp"

#include <vector>

namespace meteo::grid_deser {
using grid_array = std::vector<std::optional<grid::grid_t>>;

[[nodiscard]] grid_array deserialize_grid(std::string_view body);
}  // namespace meteo::grid_deser
