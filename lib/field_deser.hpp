#pragma once

#include "lib/field.hpp"

#include <vector>

namespace meteo::field_deser {
using field_array = std::vector<field::field_t>;

[[nodiscard]] field_array deserialize_field(std::string_view body);
}  // namespace meteo::field_deser
