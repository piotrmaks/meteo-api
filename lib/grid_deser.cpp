#include "lib/grid_deser.hpp"

#include <nlohmann/json.hpp>

#include <algorithm>

namespace meteo::grid_deser {
using json = nlohmann::json;

grid_array deserialize_grid(std::string_view body) {
  grid_array result{};
  auto parsed = json::parse(body);

  if (auto grids = parsed.find("grids"); grids != parsed.end()) {
    std::transform(
        grids->begin(), grids->end(), std::back_inserter(result),
        [](const std::string& grid) { return grid::make_grid(grid); });
  }

  return result;
}
}  // namespace meteo::grid_deser
