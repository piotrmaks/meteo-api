#pragma once

namespace meteo::point {
struct point_t {
  int row;
  int col;
};
}  // namespace meteo::point
