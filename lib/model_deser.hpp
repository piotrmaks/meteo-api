#pragma once

#include "lib/model.hpp"

#include <vector>

namespace meteo::model_deser {
using model_array = std::vector<std::optional<model::model_t>>;

[[nodiscard]] model_array deserialize_model(std::string_view body);
}  // namespace meteo::model_deser
