#pragma once

#include <string>

namespace meteo::field {
struct field_t {
  const std::string field_name;
};
}  // namespace meteo::field
