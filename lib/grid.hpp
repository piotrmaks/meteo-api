#pragma once

#include <optional>
#include <string_view>
#include <variant>

namespace meteo::grid {
struct _2a {};
struct d01_XLONG_U_XLAT_U {};
struct d01_XLONG_V_XLAT_V {};
struct d01_XLONG_XLAT {};
struct d02_XLONG_U_XLAT_U {};
struct d02_XLONG_V_XLAT_V {};
struct d02_XLONG_XLAT {};

using grid_t = std::variant<_2a,
                            d01_XLONG_U_XLAT_U,
                            d01_XLONG_V_XLAT_V,
                            d01_XLONG_XLAT,
                            d02_XLONG_U_XLAT_U,
                            d02_XLONG_V_XLAT_V,
                            d02_XLONG_XLAT>;

[[nodiscard]] std::optional<grid_t> make_grid(std::string_view grid_name);
}  // namespace meteo::grid
