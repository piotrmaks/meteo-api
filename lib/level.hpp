#pragma once

namespace meteo::level {
struct level_t {
  const int value;
};

}  // namespace meteo::level
