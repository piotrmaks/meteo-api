#include "lib/point_deser.hpp"

#include <nlohmann/json.hpp>

#include <algorithm>

namespace meteo::point_deser {
using json = nlohmann::json;

point_array deserialize_point(std::string_view body) {
  point_array result{};
  auto parsed = json::parse(body);

  if (auto points = parsed.find("points"); points != parsed.end()) {
    std::transform(points->begin(), points->end(), std::back_inserter(result),
                   [](const auto& point) {
                     return point::point_t{point.at("row"), point.at("col")};
                   });
  }

  return result;
}
}  // namespace meteo::point_deser
