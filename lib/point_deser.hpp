#pragma once

#include "lib/point.hpp"

#include <string_view>
#include <vector>

namespace meteo::point_deser {
using point_array = std::vector<point::point_t>;

[[nodiscard]] point_array deserialize_point(std::string_view body);
}  // namespace meteo::point_deser
