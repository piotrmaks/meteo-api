"""
Compiler flag configuration
"""

load(
    "//copts:copts.bzl",
    "METEO_GCC_FLAGS",
    "METEO_LLVM_FLAGS",
    "METEO_MSVC_FLAGS",
)

METEO_DEFAULT_COPTS = select({
    "//:linux": METEO_GCC_FLAGS,
    "//:osx": METEO_LLVM_FLAGS,
    "//:windows": METEO_MSVC_FLAGS,
})
