"""
List of compiler specific flags
"""

METEO_GCC_FLAGS = [
    "-std=c++17",
    "-Werror",
    "-Wall",
    "-Wextra",
    "-Wpedantic",
    "-Wdouble-promotion",
    "-Wcast-qual",
    "-Wcast-align",
    "-Wconversion",
    "-Wmissing-declarations",
    "-Wshadow",
    "-Wsign-conversion",
    "-Wnon-virtual-dtor",
    "-Wold-style-cast",
    "-Woverloaded-virtual",
    "-Wunused",
    "-Wvarargs",
    "-Wvla",
    "-Wwrite-strings",
]

METEO_LLVM_FLAGS = [
    "-std=c++17",
    "-Werror",
    "-Wall",
    "-Wextra",
    "-Wpedantic",
    "-Weverything",
    "-Wno-c++98-compat-pedantic",
    "-Wno-global-constructors",
    "-Wbitfield-enum-conversion",
    "-Wbool-conversion",
    "-Wconstant-conversion",
    "-Wenum-conversion",
    "-Wint-conversion",
    "-Wliteral-conversion",
    "-Wnull-conversion",
    "-Wobjc-literal-conversion",
    "-Wstring-conversion",
]

METEO_MSVC_FLAGS = [
    "/std:c++17",
    "/W4",
    "/WX",
]
